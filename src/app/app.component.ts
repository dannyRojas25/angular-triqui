import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-example';
  matrixNXN: Array<any> = new Array();
  duty = false;
  triqui = "";
  terminarTriqui = false;

  ngOnInit() {
    for (let i = 0; i < 3; i++) {
      this.matrixNXN.push(
        [
          {
            PosX: i,
            PosY: 0,
            wasTouched: false,
            whoTouched: '-',
          },
          {
            PosX: i,
            PosY: 1,
            wasTouched: false,
            whoTouched: '-',
          },
          {
            PosX: i,
            PosY: 2,
            wasTouched: false,
            whoTouched: '-',
          }
        ]
      );
    }
  }


  tocoItem(itemTocado) {
    if (!this.terminarTriqui) {
      if (!itemTocado.wasTouched) {
        itemTocado.wasTouched = true;
        if (this.duty) {
          itemTocado.whoTouched = 'O';
          console.log(itemTocado);
          if (this.matrixNXN[0][0].whoTouched == "O"
            && this.matrixNXN[0][1].whoTouched == "O" && this.matrixNXN[0][2].whoTouched == "O") {
              alert("Gano O");              
          }
          if (this.matrixNXN[1][0].whoTouched == "O"
            && this.matrixNXN[1][1].whoTouched == "O" && this.matrixNXN[1][2].whoTouched == "O") {
              alert("Gano O");
          }
          if (this.matrixNXN[2][0].whoTouched == "O"
            && this.matrixNXN[2][1].whoTouched == "O" && this.matrixNXN[2][2].whoTouched == "O") {
              alert("Gano O");
          }
          if (this.matrixNXN[0][0].whoTouched == "O"
            && this.matrixNXN[1][0].whoTouched == "O" && this.matrixNXN[2][0].whoTouched == "O") {
              alert("Gano O");
          }
          if (this.matrixNXN[0][1].whoTouched == "O"
            && this.matrixNXN[1][1].whoTouched == "O" && this.matrixNXN[2][1].whoTouched == "O") {
              alert("Gano O");
          }
          if (this.matrixNXN[0][2].whoTouched == "O"
            && this.matrixNXN[1][2].whoTouched == "O" && this.matrixNXN[2][2].whoTouched == "O") {
              alert("Gano O");
          }
          if (this.matrixNXN[0][0].whoTouched == "O"
            && this.matrixNXN[1][1].whoTouched == "O" && this.matrixNXN[2][2].whoTouched == "O") {
              alert("Gano O");
          }
          if (this.matrixNXN[2][0].whoTouched == "O"          
            && this.matrixNXN[1][1].whoTouched == "O" && this.matrixNXN[0][2].whoTouched == "O") {
              console.log(itemTocado.PosX);
              console.log(itemTocado.PosY);
              alert("Gano O");
          }
        } else {
          itemTocado.whoTouched = 'X';
          console.log(itemTocado);
          if (this.matrixNXN[0][0].whoTouched == "X"
            && this.matrixNXN[0][1].whoTouched == "X" && this.matrixNXN[0][2].whoTouched == "X") {
              alert("Gano X");
          }
          if (this.matrixNXN[1][0].whoTouched == "X"
            && this.matrixNXN[1][1].whoTouched == "X" && this.matrixNXN[1][2].whoTouched == "X") {
              alert("Gano X");
          }
          if (this.matrixNXN[2][0].whoTouched == "X"
            && this.matrixNXN[2][1].whoTouched == "X" && this.matrixNXN[2][2].whoTouched == "X") {
              alert("Gano X");
          }
          if (this.matrixNXN[0][0].whoTouched == "X"
            && this.matrixNXN[1][0].whoTouched == "X" && this.matrixNXN[2][0].whoTouched == "X") {
              alert("Gano X");
          }
          if (this.matrixNXN[0][1].whoTouched == "X"
            && this.matrixNXN[1][1].whoTouched == "X" && this.matrixNXN[2][1].whoTouched == "X") {
              alert("Gano X");
          }
          if (this.matrixNXN[0][2].whoTouched == "X"
            && this.matrixNXN[1][2].whoTouched == "X" && this.matrixNXN[2][2].whoTouched == "X") {
              alert("Gano X");
          }
          if (this.matrixNXN[0][0].whoTouched == "X"
            && this.matrixNXN[1][1].whoTouched == "X" && this.matrixNXN[2][2].whoTouched == "X") {
              alert("Gano X");
          }
          if (this.matrixNXN[2][0].whoTouched == "X"
            && this.matrixNXN[1][1].whoTouched == "X" && this.matrixNXN[0][2].whoTouched == "X") {
            alert("Gano X");
          }
        }
        this.duty = !this.duty;
      }      
    }
  }

}
